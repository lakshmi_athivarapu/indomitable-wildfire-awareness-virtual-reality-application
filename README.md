Motivation - The recent Woosley Wildfires in Los Angeles has burned large area and destroyed thousands of structures and killed people and prompted for evacuation of more than a quarter million population. For campers, unexpected disasters are very concerning as they are isolated.  

Introduction - This Application is for providing Wildfire Awareness to the campers and training him with safety measures. 


Here's the Project Video Link - https://www.youtube.com/watch?v=L2MGbAmiryY

As per the sequence in the Video: 

1. The camper wakes up to the fire and looks outside his tent with fright. 
2. He turns on the radio and receives a news update about wildfires in his area. 
3. He steps out of the tent. Looking at the intensity of the fire, he gets terrified. Getting into the tent back, he rushes to make an emergency call.
4. Follows the instructions given on phone and takes safety measures like covering his face with a moist cloth. 
5. As guided, he evacuates the tent to search for a safe place to hide. Looking at the intensity of fire on the ground, he climbs up the water bunker. 
6. View from the Water Bunker - Standing here, he locates a safest place to hide. The whole environment is filled with fire and smoke.
7. After the ground fire stops, he walks to a rocky area to wait till the Fire Engine comes for rescue.