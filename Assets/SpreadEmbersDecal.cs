using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
class Cordinates{
    float x;
    float y;
    public void add_save(int x, int y){
        this.x = x;
        this.y = y;
    }
}
public class SpreadEmbersDecal : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject embersDecal = null;
    public GameObject fire_movement = null;
    public GameObject ambientSparks = null;
    public GameObject edgeFire = null;
    public GameObject largeFire = null;
    public Camera myCamera;
    public Camera[] cameras;
    private int currentCameraIndex;
    int i =0;
    int j=10;
    float myTime = 0f;
    List<(float,float)>[] grass_coordinates = new List<(float,float)>[7];
    List<float>[] times_for_grass_coordinates = new List<float>[7];
    float x_rate = 1.0f;
    float z_rate = 5.0f;
    private IEnumerator coroutine1;
    private IEnumerator coroutine;
    

    void Start()
    {   
        
       
        List<(float,float)> a = new List<(float,float)>();
        a.Add((110.0f, 108.0f));
        a.Add((37.0f, 113.0f));
        grass_coordinates[0] = a;
        // print("grass_coordinates.Length");
        // Debug.Log(grass_coordinates.Length);
        // List<(float,float)> b = new List<(float,float)>();
        // b.Add((80.0f, 509.0f));
        // b.Add((232323.0f,4444322.0f));
        // grass_coordinates[1] = b;

        // Debug.LogError(grass_coordinates[0][0].Item1);
        // Debug.LogError(grass_coordinates[0][0].Item2);
        // Debug.LogError(grass_coordinates[1][0].Item1);
        // Debug.LogError(grass_coordinates[1][0].Item2);
        // Debug.LogError(grass_coordinates[1][1].Item1);
        // Debug.LogError(grass_coordinates[1][1].Item2);


        for(int i=1; i<grass_coordinates.Length; i++){
            // print("grass_coordinates[i-1].Count");
            // print(grass_coordinates[i-1].Count);
            List<(float,float)> b = new List<(float,float)>();
            List<float> b_time = new List<float>();

           for(int j=0; j<grass_coordinates[i-1].Count; j++){
               float row = grass_coordinates[i-1][j].Item1;
               float column = grass_coordinates[i-1][j].Item2;

               float x_rate = UnityEngine.Random.Range(2.0f, 14.0f);
               float z_rate = UnityEngine.Random.Range(2.0f, 8.0f);

                b.Add((row-x_rate, column-z_rate));
                b_time.Add((float)j);
                b.Add((row-x_rate, column));
                b_time.Add((float)(j+1));
                b.Add((row-x_rate, column+(2*z_rate)));
                b_time.Add((float)(j+2));
                b.Add((row, column-z_rate));
                b_time.Add((float)(j+3));
                b.Add((row, column+(2*z_rate)));
                b_time.Add((float)(j+4));
                b.Add((row+x_rate, column-z_rate));
                b_time.Add((float)(j+5));
                b.Add((row+x_rate, column));
                b_time.Add((float)(j+6));
                b.Add((row+(2*x_rate), column+(2*z_rate)));
                b_time.Add((float)(j+7));
                

            //    grass_coordinates[i][j] = (row,column);
               
            //    grass_coordinates[i][j] = (row-0.5f, column-0.5f);
            //    Debug.Log("grass_coordinates[i]");
            //    Debug.Log(grass_coordinates[i]);
            //    Console.WriteLine(grass_coordinates); 
            //    grass_coordinates[i][j] = (row-0.5f, column);
            //    grass_coordinates[i][j] = (row-0.5f, column+1.0f);
            //    grass_coordinates[i][j] = (row, column-0.5f);
            //    grass_coordinates[i][j] = (row, column+1.0f);
            //    grass_coordinates[i][j] = (row+0.5f, column-0.5f);
            //    grass_coordinates[i][j] = (row+0.5f, column);
            //    grass_coordinates[i][j] = (row+1.0f, column+1.0f);
            //    print("sdoofiddhvdnodhndovhvbhvrv");
           }
           grass_coordinates[i] = b;
           times_for_grass_coordinates[i] = b_time;
        }
        // print("grass_coordinates");
        // Debug.Log(grass_coordinates);
        // List<GameObject> intList = new List<GameObject>();
        
        
        
        Instantiate( embersDecal, new Vector3((float)2.9,(float)6.1,1),Quaternion.Euler(90, 0, 0));
        // Debug.Log("Came here!");
        Instantiate( ambientSparks, new Vector3((float)2.9,(float)6.1,1),Quaternion.Euler(90, 0, 0));
        //GameObject go = (GameObject)Instantiate(Resources.Load("PF Conifer Tall BOTD"));
        currentCameraIndex = 0;
         //Turn all cameras off, except the first default one
         for (int i=1; i<cameras.Length; i++)
         {
             cameras[i].gameObject.SetActive(false);
         }
         
         //If any cameras were added to the controller, enable the first one
         if (cameras.Length>0)
         {
             cameras[0].gameObject.SetActive (true);
             //Debug.Log ("Camera with name: " + cameras [0].camera.name + ", is now enabled");
         }
    }

    // Update is called once per frame
    void Update()
    {
        i += 1;
        // float floattime = myTime - (int) myTime;
        // int inttime =  (int) (myTime - floattime);
        // Debug.Log(myTime);
        // Debug.Log((inttime));
            j +=1;

        if(i<grass_coordinates.Length){
           for(int k=0; k<grass_coordinates[i].Count; k++){
               try{
                randomize(times_for_grass_coordinates[i][k], grass_coordinates[i][k].Item1, grass_coordinates[i][k].Item2);  
               }
               catch(Exception Ex){
                //    Debug.LogError("i" + i);
                //    Debug.LogError("k" + k);
                //    Debug.LogError(grass_coordinates[i].Count);
                //    Debug.LogError("pahipahi" + grass_coordinates[i][k].Item1);
               }
               finally{
                //    Debug.LogError("Deleted!");
                   times_for_grass_coordinates[i].RemoveAt(k);
                   grass_coordinates[i].RemoveAt(k);
               }
                  
           }
        }

            // float move_speed = 0.3f * j;
            // for(float pos=1f;pos<25;pos+=1.0f){
            //     // coroutine = WaitAndPrint(0.5f, ambientSparks, move_speed, pos );
            //     // StartCoroutine(coroutine);
                
            // //Instantiate( ambientSparks, new Vector3((float)0.9 + move_speed, (float)7,1+pos),Quaternion.Euler(90, 0, 0));
            // randomize(move_speed,pos);
            //    //Instantiate( edgeFire, new Vector3((float)0.9 + move_speed,(float)6.1,1+pos),Quaternion.Euler(90, 0, 0)); 
            // //    if( j%24 == 0){  
            // //        coroutine = WaitAndPrint(0.5f, largeFire, move_speed, pos );
            // //        StartCoroutine(coroutine);
            // //     //    Instantiate( largeFire, new Vector3((float)0.9 + move_speed,(float)6.3,1+pos),Quaternion.Euler(90, 0, 0));
            // //    } 

            // }
            
            // Debug.Log(i);
    }

    void randomize(float time, float move_speed, float pos){
        int a = UnityEngine.Random.Range(0,100);
        switch(a){
            case int n when (n<=14) : 
            coroutine = WaitAndPrint(time+10.0f,ambientSparks, move_speed, pos );
            StartCoroutine(coroutine);
            // Instantiate( ambientSparks, new Vector3((float)0.9 + move_speed,(float)6.3,1+pos),Quaternion.Euler(90, 0, 0)); 
            break;
            case int n when(n>98 && n<=100):
            coroutine = WaitAndPrint( time+5.0f, largeFire, move_speed, pos );
            StartCoroutine(coroutine);
            // Instantiate( largeFire, new Vector3((float)0.9 + move_speed,(float)6.3,1+pos),Quaternion.Euler(90, 0, 0)); 
            break;
            case int n when(n>=50 && n<=80): 
            coroutine = WaitAndPrint(time,embersDecal, move_speed, pos );
            StartCoroutine(coroutine);
            // Instantiate( largeFire, new Vector3((float)0.9 + move_speed,(float)6.3,1+pos),Quaternion.Euler(90, 0, 0)); 
            // Instantiate( embersDecal, new Vector3((float)0.9 + move_speed,(float)6.3,1+pos),Quaternion.Euler(90, 0, 0)); 
            break;
            case int n when(n>=97): 
            coroutine = WaitAndPrint(time+0.3f,fire_movement, move_speed, pos );
            StartCoroutine(coroutine);
            // Instantiate( ambientSparks, new Vector3((float)0.9 + move_speed,(float)6.3,1+pos),Quaternion.Euler(90, 0, 0));
            break;
        }

        // coroutine = WaitAndPrint(10.0f,ambientSparks, move_speed, pos );
        //     StartCoroutine(coroutine);


    }


    void provide_list(int index_for_the_array, int list_number, (float x,float y) tuple) {
        this.grass_coordinates[index_for_the_array][list_number] = (tuple.Item1,tuple.Item2);
    }

    void OnCollisionEnter(Collision collision)
    {
        // Debug-draw all contact points and normals
        // Debug.Log("1 this is oncollision" + i);

    //     foreach (ContactPoint contact in collision.contacts)
    //     {
    //         Debug.DrawRay(contact.point, contact.normal, Color.white);
    //     }

    //     // Play a sound if the colliding objects had a big impact.
    //     if (collision.relativeVelocity.magnitude > 2)
    //         Debug.Log("this is oncollision" + i);
    }

    private IEnumerator WaitAndPrint(float waitTime, object type, float move_speed, float pos)
    {
        yield return new WaitForSeconds(waitTime);
            try{
                Instantiate((GameObject) type, new Vector3((float)0.0 + move_speed,(float)22.7f,0.0f+pos),Quaternion.Euler(90, 0, 0));
            }
            catch(Exception ex){
                print(ex + "We will look into it.");
            }
        
        // print("Coroutine ended: " + Time.time + " seconds");
    }
}
