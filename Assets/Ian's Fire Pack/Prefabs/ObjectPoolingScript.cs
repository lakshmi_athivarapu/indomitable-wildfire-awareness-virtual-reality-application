using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolingScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject embersDecal = null;
    public GameObject fire_movement = null;
    public GameObject ambientSparks = null;
    public GameObject edgeFire = null;
    public GameObject largeFire = null;
    public bool willGrow = true;
    int i_pool =0;
    int j_pool = 0;
    int Count =0;
    float waitTime = 0.12f;



    public List<(float,float)>[] grass_coordinates_pool = new List<(float,float)>[3];
    public List<GameObject>[] game_objects_pool = new List<GameObject>[5];
    // List<float>[] times_for_grass_coordinates_pool = new List<float>[7];

    private static ObjectPoolingScript objectPoolingScript_i = null;

    private ObjectPoolingScript(){

    }

    public static ObjectPoolingScript GetInstance(){
        if (objectPoolingScript_i == null)
            {
                objectPoolingScript_i = new ObjectPoolingScript();
            }
            return objectPoolingScript_i;
    }
    void Start()
    {
        List<(float,float)> a = new List<(float,float)>();
        a.Add((89.0f, 75.0f));
        a.Add((89.0f, 75.0f));
        a.Add((89.0f, 75.0f));
        a.Add((88.0f, 75.0f));

        grass_coordinates_pool[0] = a;
        
        for(int c=0; c<5; c++){
            game_objects_pool[c] = new List<GameObject>();
        }
        
        
        // Instantiate_game_objects(200.0f, 135.0f);
        // Instantiate_game_objects(200.0f, 130.0f);

        for(int i=1; i<grass_coordinates_pool.Length; i++){
            List<(float,float)> b = new List<(float,float)>();
            List<float> b_time = new List<float>();

           for(int j=0; j<grass_coordinates_pool[i-1].Count; j++){
               float row = grass_coordinates_pool[i-1][j].Item1;
               float column = grass_coordinates_pool[i-1][j].Item2;

               float x_rate = UnityEngine.Random.Range(0.3f, 6.5f);
               float z_rate = UnityEngine.Random.Range(0.3f, 0.5f);

                //b.Add((row-x_rate, column-z_rate));
                //Instantiate_game_objects(row-x_rate, column-z_rate);
                //b_time.Add((float)j/10);

                b.Add((row-x_rate, column));
                Instantiate_game_objects(row-x_rate, column);
                b_time.Add((float)(j+1)/10);

                b.Add((row-x_rate, column+(2*z_rate)));
                Instantiate_game_objects(row-x_rate, column+(2*z_rate));
                b_time.Add((float)(j+2)/10);

                b.Add((row, column-z_rate));
                Instantiate_game_objects(row, column-z_rate);
                b_time.Add((float)(j+3)/10);

                b.Add((row, column+(2*z_rate)));
                Instantiate_game_objects(row, column+(2*z_rate));
                b_time.Add((float)(j+4)/10);

                b.Add((row+x_rate, column-z_rate));
                Instantiate_game_objects(row+x_rate, column-z_rate);
                b_time.Add((float)(j+5)/10);

                b.Add((row+x_rate, column));
                Instantiate_game_objects(row+x_rate, column);
                b_time.Add((float)(j+6)/10);

                b.Add((row+(2*x_rate), column+(2*z_rate)));
                Instantiate_game_objects(row+(2*x_rate), column+(2*z_rate));
                b_time.Add((float)(j+7)/10);
                
           }
           grass_coordinates_pool[i] = b;
        //    times_for_grass_coordinates[i] = b_time;
        }


    }

    private void Instantiate_game_objects(float x, float z){
        GameObject obj1 = (GameObject)Instantiate(embersDecal, new Vector3(x,23.35f,z),Quaternion.Euler(90, 0, 0));
        obj1.SetActive(false);
        GameObject obj2 = (GameObject)Instantiate(largeFire, new Vector3(x,23.5f,z),Quaternion.Euler(90, 0, 0));
        obj2.SetActive(false);
        GameObject obj3 = (GameObject)Instantiate(fire_movement, new Vector3(x,23.0f,z),Quaternion.Euler(90, 0, 0));
        obj3.SetActive(false);
        GameObject obj4 = (GameObject)Instantiate(largeFire, new Vector3(x,23.5f,z),Quaternion.Euler(90, 0, 0));
        obj4.SetActive(false);
        GameObject obj5 = (GameObject)Instantiate( ambientSparks, new Vector3(x,23.15f,z),Quaternion.Euler(90, 0, 0));
        obj5.SetActive(false);

        game_objects_pool[0].Add(obj1);
        Debug.LogError(game_objects_pool[0].Count);
        game_objects_pool[1].Add(obj2);
         Debug.LogError(game_objects_pool[1].Count);
        game_objects_pool[2].Add(obj3);
         Debug.LogError(game_objects_pool[2].Count);
        game_objects_pool[3].Add(obj4);
         Debug.LogError(game_objects_pool[3].Count);
        game_objects_pool[4].Add(obj5);
         Debug.LogError(game_objects_pool[4].Count);
    }

    public GameObject GetPooledObjects(int whichobj, float x, float y){
            for(int j=0; j < game_objects_pool[whichobj].Count;j++){
                // if(game_objects_pool[whichobj][j] == null){
                //     game_objects_pool[whichobj][j] = return_type(whichobj,x,y);
                //     game_objects_pool[whichobj][j].SetActive(false);
                //     return game_objects_pool[whichobj][j];
                // }

                if(!game_objects_pool[whichobj][j].activeInHierarchy)
                {
                    Debug.LogError("got it?");
                    return game_objects_pool[whichobj][j];
                } 
            }
        if(false){
            GameObject g_obj = return_type(whichobj,x,y);
            game_objects_pool[whichobj].Add(g_obj);
            return g_obj;
        }
        return null;
    }


    private GameObject return_type(int whichobj, float x, float z){
        GameObject g_obj ;
        switch(whichobj){
            case 0:
                g_obj = (GameObject)Instantiate( embersDecal, new Vector3(x,23.3f,z),Quaternion.Euler(90, 0, 0));
                break;
            case 1:
                g_obj = (GameObject)Instantiate( fire_movement, new Vector3(x,23.0f,z),Quaternion.Euler(90, 0, 0));
                break;
            case 2:
                g_obj = (GameObject)Instantiate(largeFire, new Vector3(x,23.0f,z),Quaternion.Euler(90, 0, 0));
                break;
            case 3:
                g_obj = (GameObject)Instantiate( largeFire, new Vector3(x,23.0f,z),Quaternion.Euler(90, 0, 0));
                break;
            case 4:
                g_obj = (GameObject)Instantiate( ambientSparks, new Vector3(x,23.85f,z),Quaternion.Euler(90, 0, 0));
                break;
            default:
                g_obj = null;
                Debug.LogError("Something is wrong. Not found gameobject");
                break;
        }
        
        return g_obj;
    }
    // Update is called once per frame
    void Update()
    {
        waitTime -= Time.deltaTime;
        if (waitTime < 0.0f)
        {
            if (game_objects_pool[i_pool].Count > j_pool)
            {
                try
                {

                    Debug.LogError(game_objects_pool[i_pool][j_pool]);
                    if (game_objects_pool[i_pool][j_pool])
                    {
                        game_objects_pool[i_pool][j_pool].SetActive(true);
                        if (i_pool != 0)
                        {
                            game_objects_pool[i_pool - 1][j_pool].SetActive(false);
                        }
                        //if (i_pool > 1)
                        //{
                        //  game_objects_pool[i_pool - 2][j_pool].SetActive(true);
                        //}
                    }

                }
                catch (UnityException ex)
                {
                    //  Debug.LogError(i_pool);
                    //  Debug.LogError(j_pool);
                }

                j_pool++;

            }
            else
            {

                if (i_pool < 4)
                {
                    Debug.LogError("came here");
                    i_pool++;
                }

                j_pool = 0;
            }
            waitTime = 0.12f;

        }

    }
}
