using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectMugRotation : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject mug_object;
    public ParticleSystem particleSystem;
    Quaternion currentEulerAngles;
    private ParticleSystem.EmissionModule particleSystemEmission;
    void Start()
    {
        //particleSystem = GetComponent<ParticleSystem>();
        particleSystemEmission = particleSystem.emission;
    }

    // Update is called once per frame
    void Update()
    {
        
        currentEulerAngles = mug_object.transform.rotation;
        //Debug.LogError(mug_object.transform.rotation);
        float temp, temp_x, temp_z;
        var main = particleSystem.main;
       
        //Debug.Log(main);

        if(currentEulerAngles.x < 0)
        {
            temp_x = currentEulerAngles.x * -1;
        }
        else
        {
            temp_x = currentEulerAngles.x;
        }
            
        // Debug.LogError(temp_x);
        if(currentEulerAngles.z < 0)
        {
            temp_z = currentEulerAngles.z * -1;
        }
        else
        {
            temp_z = currentEulerAngles.z;
        }
        // Debug.LogError(temp_z);
        if (temp_x > temp_z)
        {
            temp = temp_x;
        }
        else
        {
            temp = temp_z;
        }

        float value = 0.0f;
       
        switch(temp) {
            case float n when (n<0.1):
                Debug.LogError("1");
                value = 0.0f;
               // main.simulationSpeed = 0.07f;
               
                break;
             case float n when (n<0.2):
                value = 700.0f;
                main.simulationSpeed = 0.08f;
                break;
             case float n when (n<0.3):
                value = 700.0f;
                //main.simulationSpeed = 0.09f;
                break;
             case float n when (n<0.4):
                value = 2000.0f;
                //main.simulationSpeed = 0.1f;
                break;
             case float n when (n<0.5):
                value = 3100.0f;
                //main.simulationSpeed = 0.11f;
                break;
            case float n when(n<0.6):
               value = 4150.0f;
                //main.simulationSpeed = 0.12f;
                break;
            case float n when (n<0.7):
                value = 5000.0f;
                //main.simulationSpeed = 0.13f;
                break;
            case float n when (n<0.8):
                value = 6000.0f;
                //main.simulationSpeed = 0.15f;
                break;
            case float n when (n<0.9):
               value = 7000.0f;
                //main.simulationSpeed = 0.18f;
                break;
            case float n when (n<1.0):
                value = 0.0f;
                //main.simulationSpeed = 0.2f;
                break;
            
            default:
               value = 0.0f;
                //main.simulationSpeed = 0.07f;
                break;
        }
        try
        {
            particleSystemEmission.rateOverTime = value;
        }
        catch(Exception)
        {
            Debug.LogError(particleSystemEmission);
        }
        


    }
}
