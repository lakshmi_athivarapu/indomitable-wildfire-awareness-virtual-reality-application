﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
public class VrController : MonoBehaviour
{
    public float sensitivity = 0.1f;
    public float maxSpeed = 1.0f;

    public SteamVR_Action_Boolean MovePress = null;
    public SteamVR_Action_Vector2 MoveValue = null;

    private float Speed = 0.0f;
    private CharacterController characterController = null;
    private Transform cameraRig = null;
    private Transform head = null;


    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }
    void Start()
    {
        cameraRig = SteamVR_Render.Top().origin;
        head = SteamVR_Render.Top().head;
    }

    // Update is called once per frame
    void Update()
    {
        HandleHead();
        HandleHeight();
        CalculateMovement();
        
    }

    private void HandleHead()
    {
        Vector3 oldPosition = cameraRig.position;
        Quaternion oldRotation = cameraRig.rotation;

        transform.eulerAngles = new Vector3(0.0f, head.rotation.eulerAngles.y, 0.0f);

        cameraRig.position = oldPosition;
        cameraRig.rotation = oldRotation;

    }
    private void CalculateMovement()
    {
        Vector3 orientationEuler = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        Vector3 movement = Vector3.zero;


        if (MovePress.GetStateUp(SteamVR_Input_Sources.Any))
        {
            Speed = 0;
        }

        if (MovePress.state)
        {
            Speed += MoveValue.axis.y * sensitivity;
            Speed = Mathf.Clamp(Speed, -maxSpeed, maxSpeed);
            movement += orientation * (Speed * Vector3.forward) * Time.deltaTime;
        }
        characterController.Move(movement);
    }

    private void HandleHeight()
    {
        float headHeight = Mathf.Clamp(head.localPosition.y, 1, 2);
        characterController.height = headHeight;

        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;


        newCenter.x = head.localPosition.x;
        newCenter.z = head.localPosition.z;

        newCenter = Quaternion.Euler(0, -transform.eulerAngles.y, 0)*newCenter;
        characterController.center = newCenter;



    }
}
